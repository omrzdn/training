<?php require "init.php" ?>

<?php
//check if user coming from request
if ($_SERVER['REQUEST_METHOD'] == 'POST') {


    //Assign variables
    $username = filter_var($_POST['uname'], FILTER_SANITIZE_STRING);
    $password  = filter_var($_POST['pass'], FILTER_SANITIZE_STRING);

    // Creating Array of errors
    $formErrors = array();
    if (strlen($username) <= 3) {

        $formErrors[] = "Username Must be larger than <strong>3</strong> characters";
    }
    if (strlen($password) < 5) {

        $formErrors[] = "password Can\'t Be less Than <strong>5</strong> characters";
    }
}
?>
<main>
  <div class="main-div">
      <!-- The Login Of Page-->
          <form class="login" action="<?php echo $_SERVER['PHP_SELF']?>" method="POST">
              <h4 class="text-center">Let's Sign in</h4>
              <input
                     class="form-control"
                     type="text"
                     name="uname"
                     placeholder="Username"
                     autocomplete"off">

              <input
                     class="form-control password"
                     type="password"
                     name="pass"
                     placeholder="Password"
                     autocomplete"new-password">

              <input
                     class="checkbox"
                     type="checkbox">
              <p>Show Password</p>
              <a class="btn-signup" target="_self" href="signup.php">First Time? Signup now</a>
              <input
                     class="btn-login"
                     type="submit"
                     value="Login">
          </form>

  </div>
</main>
<?php require $tplRoute . "footer.php"; ?>
