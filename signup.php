<?php require 'init.php'; ?>


<?php

    //check if user coming from request
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {

        //Assign variables
        $user = filter_var($_POST['username'], FILTER_SANITIZE_STRING);
        $mail = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
        $pass  = filter_var($_POST['password'], FILTER_SANITIZE_STRING);

        // Creating Array of errors
        $formErrors = array();
        if (strlen($user) <= 3) {

            $formErrors[] = "Username Must be larger than <strong>3</strong> characters";
        }
        if (strlen($pass) < 5) {

            $formErrors[] = "password Can\'t Be less Than <strong>5</strong> characters";
        }
    }

?>

<main>
  <form class="signup-form" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST">
    <h2>Sign up</h2>
    <input class="form-control" type="text" name="username" placeholder="Type Your Username Here">
    <input class="form-control" type="email" name="email" placeholder="Type Your Email Here Please">
    <input class="form-control" type="password" name="pass" placeholder="Type Your Password">
    <input class="form-control" type="password" name="cpass" placeholder="Confirm Password">
    <input class="btn-login" type="submit" name="signup" value="Sign Up Now">
  </form>
</main>

<?php require $tplRoute . "footer.php"; ?>
